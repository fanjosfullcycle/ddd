FROM nginx:latest

WORKDIR '/usr/share/nginx/html'
RUN apt-get update && apt-get install vim -y && apt-get install iputils-ping -y
#RUN apt install nodejs -y
#RUN apt install npm -y
#RUN npm install -g typescript -y

#RUN npm i -D @types/jest ts-node --save-dev
#RUN npm i -D @swc/jest @swc/cli @swc/core

#RUN npm install -g n
#RUN n stable

#COPY html/ /usr/share/nginx/html

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]