import Product from "./product";

describe("Product unit test", () => {
    
    
    it("Gera erro quando id está vazio", () => {
        
        expect(() => {
            const product = new Product("", "Product 1", 100);
        }).toThrowError("Id é obrigatório");
    });


    it("Gera erro quando nome está vazio", () => {
        
        expect(() => {
            const product = new Product("1", "", 100);
        }).toThrowError("Nome é obrigatório");
    });

    it("Gera erro quando preço nao é maior que 0", () => {
        
        expect(() => {
            const product = new Product("1", "Produto 1", 0);
            const product2 = new Product("2", "Produto 2", -10);
        }).toThrowError("Preço precisa ser maior que 0");
    });

    it("Mudar nome do produto", () => {
        
        const product = new Product("1", "Produto 1", 100);
        product.changeName("Produto 2");

        expect(product.name).toBe("Produto 2");

    });

    it("Mudar nome do produto para vazio", () => {
        
        expect(() => {
            const product = new Product("1", "Produto 1", 100);
            product.changeName("");
        }).toThrowError("Nome é obrigatório");

    });

    it("Mudar preço do produto", () => {
        
        const product = new Product("1", "Produto 1", 100);
        product.changePrice(150);

        expect(product.price).toBe(150);

    });

    it("Mudar preço para 0", () => {
        
        expect(() => {
            const product = new Product("1", "Produto 1", 100);
            product.changePrice(0);
        }).toThrowError("Preço precisa ser maior que 0");

    });

    it("Mudar preço para menor que 0", () => {
        
        expect(() => {
            const product = new Product("1", "Produto 1", 100);
            product.changePrice(-20);
        }).toThrowError("Preço precisa ser maior que 0");

    });


});